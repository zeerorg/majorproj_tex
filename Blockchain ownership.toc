\contentsline {chapter}{\numberline {1}Introduction}{1}
\contentsline {section}{\numberline {1.1}Overview}{1}
\contentsline {section}{\numberline {1.2}Motivation}{3}
\contentsline {section}{\numberline {1.3}Problem Statement}{4}
\contentsline {chapter}{\numberline {2}Literature Review}{5}
\contentsline {section}{\numberline {2.1}Blockchain}{5}
\contentsline {subsection}{\numberline {2.1.1}Blockchain Characteristics}{5}
\contentsline {subsection}{\numberline {2.1.2}Types of Blockchain}{6}
\contentsline {subsubsection}{Public Blockchain}{6}
\contentsline {subsubsection}{Private Blockchain}{6}
\contentsline {subsubsection}{Hybrid Blockchains}{6}
\contentsline {subsection}{\numberline {2.1.3}Compared to Traditional Database}{7}
\contentsline {section}{\numberline {2.2}Smart contracts}{7}
\contentsline {subsection}{\numberline {2.2.1}Smart Contract platforms}{7}
\contentsline {chapter}{\numberline {3}Related Work}{9}
\contentsline {subsection}{\numberline {3.0.1}Blockchain and Public sector}{9}
\contentsline {subsection}{\numberline {3.0.2}Blockchain-based land registration}{9}
\contentsline {subsection}{\numberline {3.0.3}Other notable works}{9}
\contentsline {chapter}{\numberline {4}Proposed Work}{11}
\contentsline {section}{\numberline {4.1}System Components}{11}
\contentsline {subsection}{\numberline {4.1.1}Participanting entities}{11}
\contentsline {subsection}{\numberline {4.1.2}Contracts used}{11}
\contentsline {section}{\numberline {4.2}Proposed workflow of system}{12}
\contentsline {chapter}{\numberline {5}Implementation}{15}
\contentsline {section}{\numberline {5.1}Tools and technology used}{15}
\contentsline {section}{\numberline {5.2}Smart Contracts Code}{16}
\contentsline {subsection}{\numberline {5.2.1}Registrar Contract}{16}
\contentsline {subsection}{\numberline {5.2.2}Property Issuer Contract}{19}
\contentsline {subsection}{\numberline {5.2.3}Property Contract}{21}
\contentsline {section}{\numberline {5.3}Client's Interaction}{23}
\contentsline {subsection}{\numberline {5.3.1}Registering to registrar server}{23}
\contentsline {subsection}{\numberline {5.3.2}Registering property to issuer server}{24}
\contentsline {chapter}{\numberline {6}Conclusion and Future Work}{26}
\contentsline {chapter}{References}{27}
